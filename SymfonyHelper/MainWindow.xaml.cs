﻿using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace SymfonyHelper
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnComposer_Click(object sender, RoutedEventArgs e)
        {
            //
        }

        private void BtnCreateProject_Click(object sender, RoutedEventArgs e)
        {
            //   
        }

        private void BtnCreateProject_Click_1(object sender, RoutedEventArgs e)
        {
            string version = cbxVersion.SelectionBoxItem.ToString();
            switch (version)
            {
                case "Symfony 3.0":
                    version = "3.0";
                    break;
                case "Symfony 3.2":
                    version = "3.2";
                    break;
                case "Symfony 3.4":
                    version = "3.4";
                    break;
                case "Symfony 4.0":
                    version = "4.0";
                    break;
                case "Symfony 4.2":
                    version = "4.2";
                    break;
                default:
                    System.Windows.Forms.MessageBox.Show("Erreur lors de la selection de la version");
                    break;
            }

            if(txtURL.Text == "URL..")
            {
                System.Windows.Forms.MessageBox.Show("Please select a directory for your project", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //Process p = new Process();
                //p.StartInfo.FileName = "cmd.exe";
                //p.StartInfo.Arguments = "cd " + txtURL.Text + " /mkdir salut" ;
                //p.StartInfo.UseShellExecute = true;
                //p.Start();

                string test = "mkdir salut";
                Process.Start("cmd.exe", test);

            }


        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog browser = new FolderBrowserDialog();
            browser.ShowDialog();
            txtURL.Text = browser.SelectedPath;
        }
    }
}
